﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Services;

namespace Base64
{

    /// <summary>
    /// Descripción breve de ArchivosBase64
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    public class ArchivosBase64 : System.Web.Services.WebService
    {

        public string StringConvertedToBase64 { get; private set; }
        public string mesl = string.Empty;
        [WebMethod]
        public string HelloWorld()
        {
            return "Hola a todos";
        }

        [WebMethod]
        public string Archivos64 (string usuario,string contraseña, string Fecha,string Cliente)
        {
            string conexion1 = System.Configuration.ConfigurationManager.ConnectionStrings["=ammper"].ConnectionString;
            SqlConnection conexion = new SqlConnection(conexion1);
            string respuesta="No existe carpeta";
            string fecha1 = Fecha;
            string Cliente1 = Cliente;
            string usuario1 = usuario;
            string contrasena = contraseña;
            
            if (usuario1 == "" || fecha1 == "" || contrasena == "")
            {
                respuesta = "Por favor ingrese datos validos";
            }
            else
            {
                //Login
                string resultado = "";
                conexion.Open();
                string usuarios = "select ID_usuario from loginWeb where usuario = '"+usuario1+"' and contrasenia = '"+contrasena+"'";
                SqlCommand command = new SqlCommand(usuarios,conexion);
                SqlDataReader co = command.ExecuteReader();
                if (co.Read())
                {
                    resultado = Convert.ToString(co["ID_usuario"]); 
                }
                conexion.Close();
                if (resultado!="")
                {
                    string path = "", parti = "", fecha2 = "", fechaM = "";
                    string fechaC = "", fechaA = "", archivos = "";
                    fechaC = DateTime.Parse(fecha1).ToString("yyyy/MMMM/dd");
                    fechaA = DateTime.Parse(fecha1).ToString("yyyy");
                    fechaM = DateTime.Parse(fecha1).ToString("MM");
                    fecha2 = DateTime.Parse(fecha1).ToString("dd/MM/yyyy");
                    Regremes(fechaM);
                    conexion.Open();
                    string query = "select facturador.parti, facturador.ruta, participante.participante from facturador " +
                                    "inner join participante on facturador.parti = participante.parti " +
                                    "where facturador.parti ='" + Cliente1 + "'";
                    SqlCommand select = new SqlCommand(query, conexion);
                    SqlDataReader registro = select.ExecuteReader();
                    if (registro.Read())
                    {
                        path = Convert.ToString(registro["ruta"]);
                        parti = Convert.ToString(registro["participante"]);
                    }
                    conexion.Close();
                    if (path != "")
                    {
                        string rutaC = "", creacion1 = "", extension = "", nombre = "", fechaAr = "", fechaAr1 ="";
                        int longitud = 0;
                        string ruta = path + "\\" + parti + "\\Facturas" + "\\" + fechaA + "\\" + mesl;
                        string ruta1 = path + "\\" + parti + "\\Notas de Crédito" + "\\" + fechaA + "\\" + mesl;
                        string ruta2 = path + "\\" + parti + "\\Notas de Débito" + "\\" + fechaA + "\\" + mesl;
                        int a = 0;
                        DirectoryInfo info = new DirectoryInfo(ruta);
                        if (Directory.Exists(ruta))
                        {
                            FileInfo[] files = info.GetFiles().OrderBy(p => p.CreationTime).ToArray();
                            foreach (FileInfo file in files)
                            {
                                rutaC = ruta + "\\" + file;
                                DateTime creacion = File.GetLastWriteTime(rutaC);
                                extension = Path.GetExtension(file.ToString());
                                nombre = Path.GetFileNameWithoutExtension(file.ToString());
                                //creacion1 = creacion.ToString("dd/MM/yyyy");
                                //longitud = nombre.Length;
                                //fechaAr = nombre.Substring(0,20);
                                //fechaAr1 = fechaAr.Substring(12,20);
                                string obtainDate = nombre.Substring(12, 8);
                                string ano = obtainDate.Substring(0,4);
                                string dia = obtainDate.Substring(4, 2);
                                string mes = obtainDate.Substring(6, 2);
                                string creacionsuper = dia + "/" + mes + "/" + ano;
                                creacion1 = creacionsuper;
                                if (creacion1 == fecha2)
                                {
                                    Byte[] bytespdf = File.ReadAllBytes(rutaC);
                                    StringConvertedToBase64 = Convert.ToBase64String(bytespdf);
                                    if (extension == ".pdf")
                                    {
                                        archivos += parti + " Factura " + nombre;
                                        archivos += " PDF" + a + ":" + StringConvertedToBase64 + "  ";
                                    }
                                    else
                                    {
                                        archivos += "XML" + a + ":" + StringConvertedToBase64 + "  ";
                                    }
                                    a++;
                                }
                                respuesta = archivos;
                                if (archivos == "")
                                {
                                    respuesta = "No existen achivos";
                                }
                            }
                        }
                        //else
                        //{
                        //    respuesta += "La carpeta no existe "+path;
                        //}
                        DirectoryInfo info1 = new DirectoryInfo(ruta1);
                        if (Directory.Exists(ruta1))
                        {
                            FileInfo[] files1 = info1.GetFiles().OrderBy(p => p.CreationTime).ToArray();
                            foreach (FileInfo file1 in files1)
                            {
                                rutaC = ruta1 + "\\" + file1;
                                DateTime creacion = File.GetLastWriteTime(rutaC);
                                extension = Path.GetExtension(file1.ToString());
                                nombre = Path.GetFileNameWithoutExtension(file1.ToString());
                                //creacion1 = creacion.ToString("dd/MM/yyyy");
                                //longitud = nombre.Length;
                                //fechaAr = nombre.Substring(0,20);
                                //fechaAr1 = fechaAr.Substring(12,20);
                                string obtainDate = nombre.Substring(12, 8);
                                string ano = obtainDate.Substring(0, 4);
                                string dia = obtainDate.Substring(4, 2);
                                string mes = obtainDate.Substring(6, 2);
                                string creacionsuper = dia + "/" + mes + "/" + ano;
                                creacion1 = creacionsuper;
                                if (creacion1 == fecha2)
                                {
                                    Byte[] bytespdf = File.ReadAllBytes(rutaC);
                                    StringConvertedToBase64 = Convert.ToBase64String(bytespdf);
                                    if (extension == ".pdf")
                                    {
                                        archivos += "Archivo Notas de Crédito:  ";
                                        archivos += nombre + "  PDF" + a + ":" + StringConvertedToBase64 + "  ";
                                    }
                                    else
                                    {
                                        archivos += "XML" + a + ":" + StringConvertedToBase64 + "  ";
                                    }

                                    a++;
                                }
                                respuesta = archivos;
                                if (archivos == "")
                                {
                                    respuesta = "No existen achivos";
                                }
                            }
                        }
                        //else
                        //{
                        //    respuesta += "La carpeta no existe "+path;
                        //}
                        DirectoryInfo info2 = new DirectoryInfo(ruta2);
                        if (Directory.Exists(ruta2))
                        {
                            FileInfo[] files2 = info2.GetFiles().OrderBy(p => p.CreationTime).ToArray();
                            foreach (FileInfo file2 in files2)
                            {
                                rutaC = ruta2 + "\\" + file2;
                                DateTime creacion = File.GetLastWriteTime(rutaC);
                                extension = Path.GetExtension(file2.ToString());
                                nombre = Path.GetFileNameWithoutExtension(file2.ToString());
                                //creacion1 = creacion.ToString("dd/MM/yyyy");
                                //longitud = nombre.Length;
                                //fechaAr = nombre.Substring(0,20);
                                //fechaAr1 = fechaAr.Substring(12,20);
                                string obtainDate = nombre.Substring(12, 8);
                                string ano = obtainDate.Substring(0, 4);
                                string dia = obtainDate.Substring(4, 2);
                                string mes = obtainDate.Substring(6, 2);
                                string creacionsuper = dia + "/" + mes + "/" + ano;
                                creacion1 = creacionsuper;
                                if (creacion1 == fecha2)
                                {
                                    Byte[] bytespdf = File.ReadAllBytes(rutaC);
                                    StringConvertedToBase64 = Convert.ToBase64String(bytespdf);
                                    if (extension == ".pdf")
                                    {
                                        archivos += "Archivo Notas de Débito:  ";
                                        archivos += nombre + " PDF" + a + ":" + StringConvertedToBase64 + "  ";
                                    }
                                    else
                                    {
                                        archivos += "XML" + a + ":" + StringConvertedToBase64 + "  ";
                                    }
                                    a++;
                                }
                                respuesta = archivos;
                                if (archivos == "")
                                {
                                    respuesta = "No existen achivos";
                                }
                            }
                        }
                        //else
                        //{
                        //    respuesta += "La carpeta no existe "+path;
                        //}
                    }
                    else
                    {
                        List<string> Lista = new List<string>();
                        string co1 = "", rutaC = "", rutaC1 = "", rutaC2 = "", rutaC3 = "", extension = "", nombre = "", creacion1 = "";
                        int a = 0;
                        conexion.Open();
                        string squery = "select facturador.ruta, participante.participante from facturador " +
                                    "inner join participante on facturador.parti = participante.parti ";
                        SqlCommand selet = new SqlCommand(squery, conexion);
                        SqlDataReader registros = selet.ExecuteReader();
                        while (registros.Read())
                        {
                            co1 = registros[0].ToString();
                            Lista.Add(co1);
                            parti = Convert.ToString(registros["participante"]);
                            rutaC = co1 + "\\" + parti + "\\facturas\\" + fechaA + "\\" + mesl;
                            rutaC1 = co1 + "\\" + parti + "\\Notas de Crédito\\" + fechaA + "\\" + mesl;
                            rutaC2 = co1 + "\\" + parti + "\\Notas de Débito\\" + fechaA + "\\" + mesl;
                            DirectoryInfo info = new DirectoryInfo(rutaC);
                            if (Directory.Exists(rutaC))
                            {
                                FileInfo[] files = info.GetFiles().OrderBy(p => p.CreationTime).ToArray();
                                foreach (FileInfo file in files)
                                {
                                    rutaC3 = rutaC + "\\" + file;
                                    DateTime creacion = File.GetLastWriteTime(rutaC3);
                                    extension = Path.GetExtension(file.ToString());
                                    nombre = Path.GetFileNameWithoutExtension(file.ToString());
                                    //creacion1 = creacion.ToString("dd/MM/yyyy");
                                    //longitud = nombre.Length;
                                    //fechaAr = nombre.Substring(0,20);
                                    //fechaAr1 = fechaAr.Substring(12,20);
                                    string obtainDate = nombre.Substring(12, 8);
                                    string ano = obtainDate.Substring(0, 4);
                                    string dia = obtainDate.Substring(4, 2);
                                    string mes = obtainDate.Substring(6, 2);
                                    string creacionsuper = dia + "/" + mes + "/" + ano;
                                    creacion1 = creacionsuper;
                                    if (creacion1 == fecha2)
                                    {
                                        Byte[] bytespdf = File.ReadAllBytes(rutaC3);
                                        StringConvertedToBase64 = Convert.ToBase64String(bytespdf);
                                        if (extension == ".pdf")
                                        {
                                            archivos += parti + " Factura " + nombre;
                                            archivos += " PDF" + a + ":" + StringConvertedToBase64 + "  ";
                                        }
                                        else
                                        {
                                            archivos += " XML" + a + ":" + StringConvertedToBase64 + "  ";
                                        }
                                        a++;
                                    }
                                    respuesta = archivos;
                                    if (archivos == "")
                                    {
                                        respuesta = "No existen achivos";
                                    }
                                }
                            }
                            //else
                            //{
                            //    respuesta += "La carpeta no existe "+path;
                            //}
                            DirectoryInfo info1 = new DirectoryInfo(rutaC1);
                            if (Directory.Exists(rutaC1))
                            {
                                FileInfo[] files1 = info1.GetFiles().OrderBy(p => p.CreationTime).ToArray();
                                foreach (FileInfo file1 in files1)
                                {
                                    rutaC3 = rutaC1 + "\\" + file1;
                                    DateTime creacion = File.GetLastWriteTime(rutaC3);
                                    extension = Path.GetExtension(file1.ToString());
                                    nombre = Path.GetFileNameWithoutExtension(file1.ToString());
                                    //creacion1 = creacion.ToString("dd/MM/yyyy");
                                    //longitud = nombre.Length;
                                    //fechaAr = nombre.Substring(0,20);
                                    //fechaAr1 = fechaAr.Substring(12,20);
                                    string obtainDate = nombre.Substring(12, 8);
                                    string ano = obtainDate.Substring(0, 4);
                                    string dia = obtainDate.Substring(4, 2);
                                    string mes = obtainDate.Substring(6, 2);
                                    string creacionsuper = dia + "/" + mes + "/" + ano;
                                    creacion1 = creacionsuper;
                                    if (creacion1 == fecha2)
                                    {
                                        Byte[] bytespdf = File.ReadAllBytes(rutaC3);
                                        StringConvertedToBase64 = Convert.ToBase64String(bytespdf);
                                        if (extension == ".pdf")
                                        {
                                            archivos += parti + " Notas de Crédito " + nombre;
                                            archivos += " PDF" + a + ":" + StringConvertedToBase64 + "  ";
                                        }
                                        else
                                        {
                                            archivos += " XML" + a + ":" + StringConvertedToBase64 + "  ";
                                        }
                                        a++;
                                    }
                                    respuesta = archivos;
                                    if (archivos == "")
                                    {
                                        respuesta = "No existen achivos";
                                    }
                                }
                            }
                            //else
                            //{
                            //    respuesta += "La carpeta no existe"+path;
                            //}
                            DirectoryInfo info2 = new DirectoryInfo(rutaC2);
                            if (Directory.Exists(rutaC2))
                            {
                                FileInfo[] files2 = info2.GetFiles().OrderBy(p => p.CreationTime).ToArray();
                                foreach (FileInfo file2 in files2)
                                {
                                    rutaC3 = rutaC2 + "\\" + file2;
                                    DateTime creacion = File.GetLastWriteTime(rutaC3);
                                    extension = Path.GetExtension(file2.ToString());
                                    nombre = Path.GetFileNameWithoutExtension(file2.ToString());
                                    //creacion1 = creacion.ToString("dd/MM/yyyy");
                                    //longitud = nombre.Length;
                                    //fechaAr = nombre.Substring(0,20);
                                    //fechaAr1 = fechaAr.Substring(12,20);
                                    string obtainDate = nombre.Substring(12, 8);
                                    string ano = obtainDate.Substring(0, 4);
                                    string dia = obtainDate.Substring(4, 2);
                                    string mes = obtainDate.Substring(6, 2);
                                    string creacionsuper = dia + "/" + mes + "/" + ano;
                                    creacion1 = creacionsuper;
                                    if (creacion1 == fecha2)
                                    {
                                        Byte[] bytespdf = File.ReadAllBytes(rutaC3);
                                        StringConvertedToBase64 = Convert.ToBase64String(bytespdf);
                                        if (extension == ".pdf")
                                        {
                                            archivos += parti + " Notas de Débito " + nombre;
                                            archivos += " PDF" + a + ":" + StringConvertedToBase64 + "  ";
                                        }
                                        else
                                        {
                                            archivos += " XML" + a + ":" + StringConvertedToBase64 + "  ";
                                        }
                                        a++;
                                    }
                                    respuesta = archivos;
                                    if (archivos == "")
                                    {
                                        respuesta = "No existen achivos";
                                    }
                                }
                            }
                            //else
                            //{
                            //    respuesta += "La carpeta no existe "+path;
                            //}
                        }
                        conexion.Close();
                    }
                }
                else
                {
                    respuesta = "Contraseña y/o usuario incorrecto";
                }
            }
            return respuesta;
        }
        public string Regremes(string mes)
        {
            //string mesl = string.Empty;

            if (mes == "01")
            { mesl = "enero"; }
            if (mes == "02")
            { mesl = "febrero"; }
            if (mes == "03")
            { mesl = "marzo"; }
            if (mes == "04")
            { mesl = "abril"; }
            if (mes == "05")
            { mesl = "mayo"; }
            if (mes == "06")
            { mesl = "junio"; }
            if (mes == "07")
            { mesl = "julio"; }
            if (mes == "08")
            { mesl = "agosto"; }
            if (mes == "09")
            { mesl = "septiembre"; }
            if (mes == "10")
            { mesl = "octubre"; }
            if (mes == "11")
            { mesl = "noviembre"; }
            if (mes == "12")
            { mesl = "diciembre"; }

            return mesl;
        }

    }
}
